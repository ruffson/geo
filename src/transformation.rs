use serde::{Deserialize, Serialize};
use std::f64::consts::PI;
use std::ops::{Add, AddAssign, Div, Sub};

#[derive(Clone, Debug, Default, Copy, Serialize, Deserialize)]
pub struct LatLong {
    pub lat: f64,
    pub long: f64,
}

impl LatLong {
    pub fn new(latitude: f64, longitude: f64) -> Self {
        LatLong {
            lat: latitude,
            long: longitude,
        }
    }

    /// Calculate the distance between two coordinates
    pub fn distance(self, other: &LatLong) -> f64 {
        let diff = [self.lat - other.lat, self.long - other.long];
        (diff[0].powi(2) + diff[1].powi(2)).sqrt()
    }
}
impl Add for LatLong {
    type Output = Self;
    fn add(self, other: LatLong) -> Self {
        LatLong::new(self.lat + other.lat, self.long + other.long)
    }
}

impl AddAssign for LatLong {
    fn add_assign(&mut self, other: Self) {
        *self = LatLong::new(self.lat + other.lat, self.long + other.long)
    }
}

impl Sub for LatLong {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        LatLong::new(self.lat - other.lat, self.long - other.long)
    }
}

impl Div for LatLong {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        LatLong::new(self.lat / other.lat, self.long / other.long)
    }
}

impl Div<f64> for LatLong {
    type Output = Self;
    fn div(self, other: f64) -> Self {
        LatLong::new(self.lat / other, self.long / other)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ModelPoint {
    i: f64,
    j: f64,
    k: f64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RasterPoint {
    x: f64,
    y: f64,
    z: f64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TiePoint(RasterPoint, ModelPoint);

pub trait GeoTranslation {
    /// Translate meters to degrees
    fn meter_to_degrees(&self, meters: f64) -> f64;
    /// Translate degrees to meter
    fn degrees_to_meter(&self, degrees: f64) -> f64;
    /// Convert raster values to latlong values in degrees. Depth coordinate may be 0 for 2D data.
    fn to_latlong_deg(&self, x_pixel: f64, y_pixel: f64) -> LatLong;
    /// Convert raster values to coordinates in meter. Depth coordinate may be 0 for 2D data.
    fn to_xy_meter(&self, x_pixel: f64, y_pixel: f64) -> [f64; 3];
    /// Convert latlong coordinates in degrees to raster coordinates.
    fn to_raster_from_latlong(&self, coordinate: LatLong) -> [f64; 2];
    /// Convert coordinates in meters to raster coordinates.
    fn to_raster_from_meter(&self, i_meter: f64, j_meter: f64) -> [f64; 2];
}

// Assume for now that the linear unit is meter, angular unit is deg and that the model unit is linear/meter
// TODO: make units configurable (should make use of geotiff enums)
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EquiRectangular {
    radius_m: f64,
    pub pixel_size: [f64; 3],
    model_tie_points: Vec<TiePoint>,
}

impl EquiRectangular {
    pub fn new(radius_m: f64, pixel_size: [f64; 3], model_tie_points: Vec<f64>) -> Self {
        let mut model_points = Vec::new();

        if model_tie_points.len() > 0 && model_tie_points.len() % 6 == 0 {
            for (i, _) in model_tie_points.iter().enumerate().step_by(6) {
                let raster_point = RasterPoint {
                    x: model_tie_points[i],
                    y: model_tie_points[i + 1],
                    z: model_tie_points[i + 2],
                };
                let model_point = ModelPoint {
                    i: model_tie_points[i + 3],
                    j: model_tie_points[i + 4],
                    k: model_tie_points[i + 5],
                };

                model_points.push(TiePoint(raster_point, model_point));
            }
        } else {
            panic!("Error reading model tie points");
        }

        EquiRectangular {
            radius_m,
            pixel_size,
            model_tie_points: model_points,
        }
    }

    /// Returns a new scaled down model.
    pub fn get_scaled(&self, scaler: f64) -> Self {
        let mut scaled_points = Vec::with_capacity(self.model_tie_points.len());
        for point in &self.model_tie_points {
            scaled_points.push(TiePoint(
                RasterPoint {
                    x: point.0.x * scaler,
                    y: point.0.y * scaler,
                    z: point.0.z * scaler,
                },
                ModelPoint {
                    i: point.1.i,
                    j: point.1.j,
                    k: point.1.k,
                },
            ));
        }
        EquiRectangular {
            radius_m: self.radius_m,
            pixel_size: [
                self.pixel_size[0] / scaler,
                self.pixel_size[1] / scaler,
                self.pixel_size[2] / scaler,
            ],
            model_tie_points: scaled_points,
        }
    }

    /// Get model with tie poits wich are offset by the given pixel offset.
    pub fn get_offset(&self, raster_offset: Vec<u32>) -> Self {
        let mut offset_point = Vec::with_capacity(self.model_tie_points.len());
        for point in &self.model_tie_points {
            let offset_raster = RasterPoint {
                x: point.0.x + raster_offset[0] as f64,
                y: point.0.y + raster_offset[1] as f64,
                z: point.0.z + raster_offset[2] as f64,
            };
            // the j direction increases southwards
            let offset_model = ModelPoint {
                i: point.1.i + offset_raster.x * self.pixel_size[0],
                j: point.1.j - offset_raster.y * self.pixel_size[1],
                k: point.1.k + offset_raster.z * self.pixel_size[2],
            };
            let tiepoint = TiePoint(offset_raster, offset_model);
            offset_point.push(tiepoint);
        }
        EquiRectangular {
            radius_m: self.radius_m,
            pixel_size: self.pixel_size,
            model_tie_points: offset_point,
        }
    }

    fn get_circumference(&self) -> f64 {
        self.radius_m * 2. * PI
    }

    pub fn empty() -> Self {
        EquiRectangular {
            radius_m: 0.,
            pixel_size: [0., 0., 0.],
            model_tie_points: vec![],
        }
    }
}

impl GeoTranslation for EquiRectangular {
    fn meter_to_degrees(&self, meters: f64) -> f64 {
        meters * (360. / self.get_circumference())
    }

    fn degrees_to_meter(&self, degrees: f64) -> f64 {
        degrees * (self.get_circumference() / 360.)
    }

    fn to_latlong_deg(&self, x_pixel: f64, y_pixel: f64) -> LatLong {
        let in_meters = self.to_xy_meter(x_pixel, y_pixel);
        // latitude corresponds to y an longitude to x
        LatLong {
            lat: self.meter_to_degrees(in_meters[1]),
            long: self.meter_to_degrees(in_meters[0]),
        }
    }

    fn to_xy_meter(&self, x_pixel: f64, y_pixel: f64) -> [f64; 3] {
        let x_meter = self.model_tie_points[0].1.i + (x_pixel * self.pixel_size[0]);
        let y_meter = self.model_tie_points[0].1.j - (y_pixel * self.pixel_size[1]);
        [x_meter, y_meter, 0.]
    }

    fn to_raster_from_latlong(&self, coordinate: LatLong) -> [f64; 2] {
        self.to_raster_from_meter(
            self.degrees_to_meter(coordinate.long),
            self.degrees_to_meter(coordinate.lat),
        )
    }

    fn to_raster_from_meter(&self, i_meter: f64, j_meter: f64) -> [f64; 2] {
        let x_raster = (i_meter - self.model_tie_points[0].1.i) / self.pixel_size[0];
        let y_raster = (self.model_tie_points[0].1.j - j_meter) / self.pixel_size[1];
        [x_raster, y_raster]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_equi_rectangular() {
        let radius = 3396190.0;
        let pixel_size = [5., 5., 0.];
        let model_tie_point = vec![0.0, 0.0, 0.0, -948400.0, 592750.5, 0.0];

        let img_size = vec![23710., 23710.];

        let upper_left_m = [-948400.0, 592750.5, 0.];
        let lower_left_m = [-948400.0, 474200.5, 0.];
        let upper_right_m = [-829850.0, 592750.5, 0.];
        let lower_right_m = [-829850.0, 474200.5, 0.];
        let center_m = [-889125.0, 533475.5, 0.];

        let upper_left_deg = LatLong::new(10.00006, -16.00008);
        let lower_left_deg = LatLong::new(8.00005, -16.00008);
        let upper_right_deg = LatLong::new(10.00006, -14.00007);
        let lower_right_deg = LatLong::new(8.00005, -14.00007);
        let center_deg = LatLong::new(9.000056, -15.00008);

        let upper_left_pixel = [0., 0.];
        let lower_left_pixel = [0., img_size[1]];
        let upper_right_pixel = [img_size[0], 0.];
        let lower_right_pixel = [img_size[0], img_size[1]];
        let center_pixel = [img_size[0] / 2., img_size[1] / 2.];

        let model = EquiRectangular::new(radius, pixel_size, model_tie_point);

        // //////////
        // raster -> degrees
        // //////////
        let latlong = model.to_latlong_deg(upper_left_pixel[0], upper_left_pixel[1]);
        assert!(latlong.lat - upper_left_deg.lat < 1e-5);
        assert!(latlong.long - upper_left_deg.long < 1e-5);

        let latlong = model.to_latlong_deg(lower_left_pixel[0], lower_left_pixel[1]);
        assert!(latlong.lat - lower_left_deg.lat < 1e-5);
        assert!(latlong.long - lower_left_deg.long < 1e-5);

        let latlong = model.to_latlong_deg(upper_right_pixel[0], upper_right_pixel[1]);
        assert!(latlong.lat - upper_right_deg.lat < 1e-5);
        assert!(latlong.long - upper_right_deg.long < 1e-5);

        let latlong = model.to_latlong_deg(lower_right_pixel[0], lower_right_pixel[1]);
        assert!(latlong.lat - lower_right_deg.lat < 1e-5);
        assert!(latlong.long - lower_right_deg.long < 1e-5);

        let latlong = model.to_latlong_deg(center_pixel[0], center_pixel[1]);
        assert!(latlong.lat - center_deg.lat < 1e-5);
        assert!(latlong.long - center_deg.long < 1e-5);

        // //////////
        // raster -> meter
        // //////////

        for (i, val) in model
            .to_xy_meter(upper_left_pixel[0], upper_left_pixel[1])
            .iter()
            .enumerate()
        {
            assert_eq!(*val, upper_left_m[i]);
        }

        for (i, val) in model
            .to_xy_meter(lower_left_pixel[0], lower_left_pixel[1])
            .iter()
            .enumerate()
        {
            assert_eq!(*val, lower_left_m[i]);
        }

        for (i, val) in model
            .to_xy_meter(upper_right_pixel[0], upper_right_pixel[1])
            .iter()
            .enumerate()
        {
            assert_eq!(*val, upper_right_m[i]);
        }

        for (i, val) in model
            .to_xy_meter(lower_right_pixel[0], lower_right_pixel[1])
            .iter()
            .enumerate()
        {
            assert_eq!(*val, lower_right_m[i]);
        }

        for (i, val) in model
            .to_xy_meter(center_pixel[0], center_pixel[1])
            .iter()
            .enumerate()
        {
            assert_eq!(*val, center_m[i]);
        }
        // //////////
        // degrees -> raster
        // //////////

        let raster = model.to_raster_from_latlong(upper_left_deg);
        println!("Raster: {:?}\n truth: {:?}", raster, upper_left_pixel);
        assert!(raster[0] - upper_left_pixel[0] < 1e-1);
        assert!(raster[1] - upper_left_pixel[1] < 1e-1);

        let raster = model.to_raster_from_latlong(lower_left_deg);
        assert!(raster[0] - lower_left_pixel[0] < 1e-1);
        assert!(raster[1] - lower_left_pixel[1] < 1e-1);

        let raster = model.to_raster_from_latlong(upper_right_deg);
        assert!(raster[0] - upper_right_pixel[0] < 1e-1);
        assert!(raster[1] - upper_right_pixel[1] < 1e-1);

        let raster = model.to_raster_from_latlong(lower_right_deg);
        assert!(raster[0] - lower_right_pixel[0] < 1e-1);
        assert!(raster[1] - lower_right_pixel[1] < 1e-1);

        let raster = model.to_raster_from_latlong(center_deg);
        assert!(raster[0] - center_pixel[0] < 1e-1);
        assert!(raster[1] - center_pixel[1] < 1e-1);

        // //////////
        // meter -> raster
        // //////////
        for (i, val) in model
            .to_raster_from_meter(upper_left_m[0], upper_left_m[1])
            .iter()
            .enumerate()
        {
            assert!(*val - upper_left_pixel[i] < 1e-5);
        }

        for (i, val) in model
            .to_raster_from_meter(lower_left_m[0], lower_left_m[1])
            .iter()
            .enumerate()
        {
            assert!(*val - lower_left_pixel[i] < 1e-5);
        }

        for (i, val) in model
            .to_raster_from_meter(upper_right_m[0], upper_right_m[1])
            .iter()
            .enumerate()
        {
            assert!(*val - upper_right_pixel[i] < 1e-5);
        }

        for (i, val) in model
            .to_raster_from_meter(lower_right_m[0], lower_right_m[1])
            .iter()
            .enumerate()
        {
            assert!(*val - lower_right_pixel[i] < 1e-5);
        }

        for (i, val) in model
            .to_raster_from_meter(center_m[0], center_m[1])
            .iter()
            .enumerate()
        {
            assert!(*val - center_pixel[i] < 1e-5);
        }
    }
}
