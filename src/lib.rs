use error::GeoError;
use geotiff::geokey::{GeoKey, GeoKeyType};
use std::collections::HashMap;
pub mod error;
pub mod geotiff;
pub mod transformation;

pub fn get_geokeydir(
    geokeydir: Vec<u16>,
    ascii_params: Option<String>,
    double_params: Option<Vec<f64>>,
) -> Result<HashMap<GeoKey, GeoKeyType>, GeoError> {
    geotiff::geokey::parse_geo_keys(geokeydir, ascii_params, double_params)
}
