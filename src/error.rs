use std::fmt;

#[derive(Debug)]
pub enum GeoError {
    GeoTiffError(GeoTiffError),
    CoordinateTransformError,
}

#[derive(Debug)]
pub enum GeoTiffError {
    VersionMismatch(u16),
    EmptyGeoDir,
    FormatError(String),
}

impl fmt::Display for GeoTiffError {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        use self::GeoTiffError::*;
        match *self {
            EmptyGeoDir => write!(fmt, "Geokey directory is empty or corrupted."),
            VersionMismatch(ref version) => write!(
                fmt,
                "Expected geo key directory version to be 1, but found {}",
                version
            ),
            FormatError(ref description) => write!(fmt, "Format Error: {}", description),
        }
    }
}
