use std::{collections::HashMap, convert::TryFrom};

use crate::error::{GeoError, GeoTiffError};
// use tags::Tag;

macro_rules! enum_try_from {
    {#[$enum_attr:meta] $vis:vis enum $tyname:ident { $($name:ident = $value:expr),* $(,)* }} => {
        #[$enum_attr]
        $vis enum $tyname {
            $($name = $value,)*
        }

        impl ::std::convert::TryFrom<u16> for $tyname {
            type Error = ();
            /// Get enum key for value
            fn try_from(num: u16) -> Result<Self, ()> {
                $(
                    if num == $value {
                        Ok($tyname::$name)
                    } else
                )* {
                    Err(())
                }
            }
        }
    };
}

// More info about geotiff keys can be found
// [here](http://geotiff.maptools.org/spec/geotiff2.7.html)
enum_try_from! {
    #[derive(Debug, PartialEq, Eq, Hash)]
    pub enum GeoKey {
    // GeoTIFF Configuration GeoKeys
    GTModelTypeGeoKey = 1024,
    GTRasterTypeGeoKey = 1025,
    GTCitationGeoKey = 1026,
    // Geographic CS Parameter GeoKeys
    GeographicTypeGeoKey = 2048,
    GeogCitationGeoKey = 2049,
    GeogGeodeticDatumGeoKey = 2050,
    GeogPrimeMeridianGeoKey = 2051,
    GeogLinearUnitsGeoKey = 2052,
    GeogLinearUnitSizeGeoKey = 2053,
    GeogAngularUnitsGeoKey = 2054,
    GeogAngularUnitSizeGeoKey = 2055,
    GeogEllipsoidGeoKey = 2056,
    GeogSemiMajorAxisGeoKey = 2057,
    GeogSemiMinorAxisGeoKey = 2058,
    GeogInvFlatteningGeoKey = 2059,
    GeogAzimuthUnitsGeoKey = 2060,
    GeogPrimeMeridianLongGeoKey = 2061,
    // Projected CS Parameter GeoKeys
    ProjectedCSTypeGeoKey = 3072,
    PCSCitationGeoKey = 3073,
    // Projection Definition GeoKeys
    ProjectionGeoKey = 3074,
    ProjCoordTransGeoKey = 3075,
    ProjLinearUnitsGeoKey = 3076,
    ProjLinearUnitSizeGeoKey = 3077,
    ProjStdParallel1GeoKey = 3078,
    ProjStdParallel2GeoKey = 3079,
    ProjNatOriginLongGeoKey = 3080,
    ProjNatOriginLatGeoKey = 3081,
    ProjFalseEastingGeoKey = 3082,
    ProjFalseNorthingGeoKey = 3083,
    ProjFalseOriginLongGeoKey = 3084,
    ProjFalseOriginLatGeoKey = 3085,
    ProjFalseOriginEastingGeoKey = 3086,
    ProjFalseOriginNorthingGeoKey = 3087,
    ProjCenterLongGeoKey = 3088,
    ProjCenterLatGeoKey = 3089,
    ProjCenterEastingGeoKey = 3090,
    ProjCenterNorthingGeoKey = 3091,
    ProjScaleAtNatOriginGeoKey = 3092,
    ProjScaleAtCenterGeoKey = 3093,
    ProjAzimuthAngleGeoKey = 3094,
    ProjStraightVertPoleLongGeoKey = 3095,
    // Vertical CS Parameter Keys
    VerticalCSTypeGeoKey = 4096,
    VerticalCitationGeoKey = 4097,
    VerticalDatumGeoKey = 4098,
    VerticalUnitsGeoKey = 4099,
}
}
#[derive(Clone, Debug, PartialEq)]
pub enum GeoKeyType {
    Short(u16),
    Double(Vec<f64>),
    Ascii(String),
    ShortVec(Vec<u16>),
}

pub fn get_geo_key(geodir: HashMap<GeoKey, GeoKeyType>, key: &GeoKey) -> Option<GeoKeyType> {
    let res = geodir.get(key);

    return match res {
        Some(val) => Some(val.clone()),
        None => None,
    };
}

pub fn parse_geo_keys(
    geokey_dir: Vec<u16>,
    ascii_params: Option<String>,
    double_params: Option<Vec<f64>>,
) -> Result<HashMap<GeoKey, GeoKeyType>, GeoError> {
    let mut geodir: HashMap<GeoKey, GeoKeyType> = HashMap::new();
    if geokey_dir.len() < 4 {
        return Err(GeoError::GeoTiffError(GeoTiffError::EmptyGeoDir));
    }

    // TODO: Save version revisions somewhere.
    let key_directory_version = geokey_dir[0];
    if key_directory_version != 1 {
        return Err(GeoError::GeoTiffError(GeoTiffError::VersionMismatch(
            key_directory_version,
        )));
    }
    let _key_revision = geokey_dir[1];
    let _key_minor_revision = geokey_dir[2];

    let num_keys = geokey_dir[3] as usize;

    // minimum size of geokeydir is 4 for version info and 4 entries per entry in geokeydir (num_keys)
    let min_size_expected = 4 + num_keys * 4;
    if geokey_dir.len() < min_size_expected {
        return Err(GeoError::GeoTiffError(GeoTiffError::FormatError(format!(
            "Geokey directory length is too short to hold all key entries."
        ))));
    }

    for i in 0..num_keys {
        let idx = 4 + i * 4;
        // Each keyEntry is made up of SHORTS: KeyID, TIFFTagLocation, Count, Value_Offset
        let key_id = match GeoKey::try_from(geokey_dir[idx]) {
            Ok(value) => value,
            Err(_) => {
                continue;
            }
        };
        let tiff_tag_location = geokey_dir[idx + 1];
        let count = geokey_dir[idx + 2];
        let value_offset = geokey_dir[idx + 3];

        if tiff_tag_location == 0 {
            geodir.insert(key_id, GeoKeyType::Short(value_offset as u16));
        } else if tiff_tag_location == 34736 {
            // should be accessed rather by Tag::GeoDoubleParamsTag
            match &double_params {
                Some(double_params) => {
                    // TODO: Check for overflow
                    let start = value_offset as usize;
                    let stop = (value_offset + count) as usize;

                    if let Some(value) = double_params.get(start..stop) {
                        geodir.insert(key_id, GeoKeyType::Double(value.to_owned()));
                    } else {
                        // Handle the invalid indices as an error maybe?
                        return Err(GeoError::GeoTiffError(GeoTiffError::FormatError(format!(
                            "Cannot read from geokey double parameters between in range[{}, {}]",
                            start, stop,
                        ))));
                    }
                }
                None => continue,
            }
        } else if tiff_tag_location == 34737 {
            // should be accessed rather by Tag::GeoAsciiParamsTag
            match &ascii_params {
                Some(ascii_params) => {
                    let start = value_offset as usize;
                    let stop = (value_offset + count) as usize;

                    if start > ascii_params.len() || stop > ascii_params.len() {
                        continue;
                    }
                    let value = String::from(&ascii_params[start..stop]);
                    geodir.insert(key_id, GeoKeyType::Ascii(value));
                }
                None => continue,
            }
        } else if tiff_tag_location == 34735 {
            // should be accessed rather by Tag::GeoKeyDirectoryTag
            // If the tag is the same as the GeoKeyDirectoryTag, the value_offset represents
            // SHORT values at the end of the `geokey_dir` itself.
            let start = value_offset as usize;
            let stop = (value_offset + count) as usize;

            if start > geokey_dir.len() || stop > geokey_dir.len() {
                continue;
            }

            let mut value = vec![0; count as usize];
            value.copy_from_slice(&geokey_dir[start..stop]);
            geodir.insert(key_id, GeoKeyType::ShortVec(value));
        } else {
            // undefined
            continue;
        }
    }
    Ok(geodir)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn geokey_parsing() {
        let geokey_dir = vec![
            1, 1, 0, 20, 1024, 0, 1, 1, 1025, 0, 1, 1, 1026, 34737, 8, 0, 2048, 0, 1, 32767, 2049,
            34737, 83, 8, 2050, 0, 1, 32767, 2054, 0, 1, 9102, 2056, 0, 1, 32767, 2057, 34736, 1,
            5, 2058, 34736, 1, 6, 2061, 34736, 1, 7, 3072, 0, 1, 32767, 3074, 0, 1, 32767, 3075, 0,
            1, 17, 3076, 0, 1, 9001, 3078, 34736, 1, 2, 3082, 34736, 1, 3, 3083, 34736, 1, 4, 3088,
            34736, 1, 1, 3089, 34736, 1, 0,
        ];
        let ascii_params = String::from(
            "unnamed|GCS Name = unnamed ellipse|Datum = unknown|Ellipsoid = unnamed|\
            Primem = Greenwich||",
        );

        let double_params = vec![0.0, 0.0, 0.0, 0.0, 0.0, 3396190.0, 3396190.0, 0.0];

        let geokeydir = parse_geo_keys(geokey_dir, Some(ascii_params), Some(double_params));

        match geokeydir {
            Ok(dir) => {
                assert_eq!(
                    *dir.get(&GeoKey::ProjectedCSTypeGeoKey).unwrap(),
                    GeoKeyType::Short(32767)
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogPrimeMeridianLongGeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjCenterLatGeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogSemiMinorAxisGeoKey).unwrap(),
                    GeoKeyType::Double(vec![3396190.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GTCitationGeoKey).unwrap(),
                    GeoKeyType::Ascii(String::from("unnamed|"))
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjCoordTransGeoKey).unwrap(),
                    GeoKeyType::Short(17)
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjLinearUnitsGeoKey).unwrap(),
                    GeoKeyType::Short(9001)
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjCenterLongGeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeographicTypeGeoKey).unwrap(),
                    GeoKeyType::Short(32767)
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjectionGeoKey).unwrap(),
                    GeoKeyType::Short(32767)
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogGeodeticDatumGeoKey).unwrap(),
                    GeoKeyType::Short(32767)
                );
                assert_eq!(
                    *dir.get(&GeoKey::GTRasterTypeGeoKey).unwrap(),
                    GeoKeyType::Short(1)
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogEllipsoidGeoKey).unwrap(),
                    GeoKeyType::Short(32767)
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjStdParallel1GeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogCitationGeoKey).unwrap(),
                    GeoKeyType::Ascii(String::from("GCS Name = unnamed ellipse|Datum = unknown|Ellipsoid = unnamed|Primem = Greenwich||"))
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjFalseNorthingGeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GTModelTypeGeoKey).unwrap(),
                    GeoKeyType::Short(1)
                );
                assert_eq!(
                    *dir.get(&GeoKey::ProjFalseEastingGeoKey).unwrap(),
                    GeoKeyType::Double(vec![0.0])
                );
                assert_eq!(
                    *dir.get(&GeoKey::GeogSemiMajorAxisGeoKey).unwrap(),
                    GeoKeyType::Double(vec![3396190.0])
                );
            }
            Err(_) => panic!("Error parsing geo key directory"),
        }
    }
}
